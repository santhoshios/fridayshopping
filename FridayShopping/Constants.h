//
//  Constants.h
//  FridayShopping
//
//  Created by WeKanCode on 27/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define themeColor [UIColor colorWithRed:183.0/255.0 green:51.0/255.0 blue:40.0/255.0 alpha:1.0]

#define BASE_URL @"http://aammtoman.com"

#define BANNER_BASE_URL @"http://aammtoman.com/admin/banner/"

#define CATEGORY_BASE_URL @"http://aammtoman.com/admin/uploads/category/"



    //======================================================================

#define GET_HOME_PAGE @"/app_webservice/gethomepage.php"



    //======================================================================

typedef enum LogState
{
    LOGGER_DEBUG,
    LOGGER_ERROR
} LogState;


#endif /* Constants_h */

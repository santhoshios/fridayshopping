//
//  CoreDataStore.m
//  Hawkeye
//
//  Created by WeKanCode on 06/09/16.
//  Copyright © 2016 WeKanCode. All rights reserved.
//


#import <CoreData/CoreData.h>


@interface CoreDataStore : NSObject

@property (nonatomic,strong,readonly) NSManagedObjectContext* mainManagedObjectContext;
@property (nonatomic,strong) NSPersistentStoreCoordinator* persistentStoreCoordinator;
@property (nonatomic,strong) NSManagedObjectModel* managedObjectModel;

- (NSManagedObjectContext*)newPrivateContext;
+ (CoreDataStore*)sharedObject;
- (NSURL *)applicationDocumentsDirectory;
-(void)flushDatabase;
//- (void)deleteEntities;

@end

//
//  ViewController.h
//  FridayShopping
//
//  Created by WeKanCode on 27/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController


typedef enum viewControllers{
    vc_productList = 0
}screenName;


- (void)pushViewController:(screenName)viewController;

@end


//
//  HomeViewController.m
//  FridayShopping
//
//  Created by WeKanCode on 27/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import "HomeViewController.h"
#import "BannerView.h"
#import "ProductCetegoriesCell.h"
#import "Category+CoreDataClass.h"

@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    
    NSMutableArray  *categoryDataArray;
    
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_CategoryListCollectionView setDelegate:self];
    [_CategoryListCollectionView setDataSource:self];
    [_CategoryListCollectionView setLayoutMargins:UIEdgeInsetsZero];
    [_CategoryListCollectionView setScrollIndicatorInsets:UIEdgeInsetsZero];
    [_CategoryListCollectionView registerClass:[BannerView
                                                class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BannerView"];
    [_CategoryListCollectionView registerNib:[UINib nibWithNibName:@"ProductCetegoriesCell" bundle:nil] forCellWithReuseIdentifier:@"ProductCell"];
        //        [_CategoryListCollectionView registerClass:[ProductCetegoriesCell class] forCellWithReuseIdentifier:@"ProductCell"];
    [self getCategories];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    UIBarButtonItem *btnUserProfile = [[UIBarButtonItem alloc]initWithTitle:@"Profile" style:UIBarButtonItemStylePlain target:self action:@selector(btnProfileClicked)];
    UIBarButtonItem *btnSearch  = [[UIBarButtonItem alloc]initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(btnSearchClicked)];
    UIBarButtonItem *btnMenu = [[UIBarButtonItem alloc]initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(btnMenuClicked)];
    
    self.navigationController.navigationItem.leftBarButtonItems = @[btnUserProfile];
    self.navigationController.navigationItem.rightBarButtonItems = @[btnSearch,btnMenu];
    
    
}

- (void)initVariables{
    
}

- (void)btnProfileClicked{
    
}

- (void)btnSearchClicked{
    
}

- (void)btnMenuClicked{
    
}

#pragma mark - ServiceCall

- (void)getCategories{
    
    [JSONHELPER getWithParams:@{} andPostMethod:GET_HOME_PAGE onCompletion:^(NSDictionary *json) {
        
    } onFailure:^(NSString *errorMessage) {
        
    }];
}

- (void)parseSaveToCOreData:(NSArray*)categoryArray{
    
    for (NSDictionary *dict in categoryArray) {
        
    }
    
    
}


#pragma mark - Collection view Datasource & Delegate Methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(self.view.frame.size.width, 100);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        
        BannerView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BannerView" forIndexPath:indexPath];
        reusableView.bannerDataArray = [NSArray new];
        [reusableView setBackgroundColor:[UIColor yellowColor]];
        return reusableView;
        
    }
    
    return nil;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 15;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Category    *categy = [categoryDataArray objectAtIndex:indexPath.row];
    
    ProductCetegoriesCell *cell = (ProductCetegoriesCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCell" forIndexPath:indexPath];
        [cell.imgCetegoryIndicator setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",CATEGORY_BASE_URL,categy.categoryImage]]];
        //[cell.lblCategoryName setText:categy.categoryName];
        //[cell setBackgroundColor:[UIColor grayColor]];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size = CGSizeMake(self.view.frame.size.width/2 - 10, (self.view.frame.size.width/2 - 20) - ((self.view.frame.size.width/2 - 20) * 1/3 ));
    return size;
}

@end

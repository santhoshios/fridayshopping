//
//  HomeViewController.h
//  FridayShopping
//
//  Created by WeKanCode on 27/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeViewController : BaseViewController


@property (weak, nonatomic) IBOutlet UICollectionView *CategoryListCollectionView;

@end

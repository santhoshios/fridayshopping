//
//  ViewController.m
//  FridayShopping
//
//  Created by WeKanCode on 27/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)pushViewController:(screenName)viewController{
    
    UIViewController* destViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    switch (viewController) {
        case vc_productList:
            destViewController = [storyboard instantiateViewControllerWithIdentifier:@"ProductListViewController"];
            break;
            
        default:
            break;
    }
    
    [(UINavigationController*)self.tabBarController.selectedViewController pushViewController:destViewController animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

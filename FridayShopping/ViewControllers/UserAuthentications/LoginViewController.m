//
//  LoginViewController.m
//  FridayShopping
//
//  Created by WeKanCode on 28/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnForgotPasswordClicked:(id)sender {
    
    if (_btnForgotPwd.isSelected) {
        
        [_btnForgotPwd setSelected:NO];
        
        [_forgotPwdView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [_forgotPwdView removeFromSuperview];
        
        [UIView animateWithDuration:0.3 animations:^{
            _loginCenterViewHeight.constant = 360;
            [self.view layoutIfNeeded];
        }];
        
    }else{
        
        [_btnForgotPwd setSelected:YES];
        
        [_forgotPwdView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [_loginCenterView addSubview:_forgotPwdView];
        
        [_forgotPwdView.topAnchor constraintEqualToAnchor:_btnForgotPwd.bottomAnchor constant:30].active = YES;
        [_forgotPwdView.centerXAnchor constraintEqualToAnchor:_loginCenterView.centerXAnchor].active = YES;
        [_forgotPwdView.widthAnchor constraintEqualToAnchor:_loginCenterView.widthAnchor].active = YES;
        [_forgotPwdView.heightAnchor constraintEqualToConstant:100].active = YES;
        _loginCenterViewHeight.constant = 460;
            //        [UIView animateWithDuration:0.3 animations:^{
//            
//            [self.view layoutIfNeeded];
//        }];
        
    }
    
    
}
@end

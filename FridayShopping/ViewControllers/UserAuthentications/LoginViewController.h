//
//  LoginViewController.h
//  FridayShopping
//
//  Created by WeKanCode on 28/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIView *forgotPwdView;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPwd;
@property (weak, nonatomic) IBOutlet UIView *loginCenterView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginCenterViewHeight;
- (IBAction)btnForgotPasswordClicked:(id)sender;


@end

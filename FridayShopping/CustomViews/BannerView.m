//
//  BannerView.m
//  FridayShopping
//
//  Created by WeKanCode on 28/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import "BannerView.h"
#import "Banner+CoreDataClass.h"

@implementation BannerView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    currentFrame = frame;
    
    return self;
}

-(NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return [_bannerDataArray count];
}

-(UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    
    Banner  *banner = [_bannerDataArray objectAtIndex:index];
    
    UIImageView *image;
    [image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BANNER_BASE_URL,banner.imageName]]];
    
    return image;
}

-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    
}


@end

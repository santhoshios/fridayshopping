//
//  BannerView.h
//  FridayShopping
//
//  Created by WeKanCode on 28/03/17.
//  Copyright © 2017 WeKanCode. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BannerView : UICollectionReusableView<iCarouselDelegate,iCarouselDataSource>{
    iCarousel           *bannerCarousel;
    CGRect              currentFrame;
}

@property (nonatomic, strong) NSArray           *bannerDataArray;

@end

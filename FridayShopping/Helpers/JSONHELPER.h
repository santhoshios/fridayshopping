//
//  JSON.h
//  deliveryBoyTracking
//
//  Created by Media3 on 5/26/16.
//  Copyright © 2016 Media3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageView+AFNetworking.h"
#import "ParserHelper.h"

typedef void (^JSONResponseBlock)(NSDictionary* json);
@interface JSONHELPER : NSObject<NSURLSessionDelegate,NSURLSessionDataDelegate>

+(void)onCompletion:(JSONResponseBlock)completionBlock;

+(void)postWithParams:(NSDictionary *)inParams andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure;

+(void)postWithParams:(NSDictionary*)inParams andImageData:(NSData *)imageData andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure;
    
+(void)getWithParams:(NSDictionary *)inParams andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure;

+(void)uploadImageWithData:(NSData *)inParams andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure;

+(void)getImageFromApi:(UIImageView *)imgView imageUrl:(NSString*)imgURLString;

+(void)cancelHttpRequests;

@end

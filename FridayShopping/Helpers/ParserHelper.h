//
//  ParserHelper.h
//  OneLeague
//
//  Created by Wekancode Mohammed on 10/05/16.
//  Copyright © 2016 Wekancode Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParserHelper : NSObject

+(id)sharedInstance;

/*!
 @brief get the non null value of from the given 'dictionary' for 'key' else returns enmpty value ""
 @return string value for the key
 */
-(NSString*)getStringFrom:(NSDictionary*)dict forKey:(NSString*)key className:(NSString*)className;

typedef void (^APIResponseSuccess)(NSDictionary *responseDictionary);
typedef void (^APIResponseFailed)(NSString *errorMessage);

/*! @brief check whether the response status is success / failure */
-(void)checkResponseStatus:(id)responseObject onSuccess:(APIResponseSuccess)success onFailure:(APIResponseFailed)failure ;
@end

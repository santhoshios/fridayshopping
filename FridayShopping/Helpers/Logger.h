//
//  Logger.h
//  OneLeague
//
//  Created by Wekancode Mohammed on 19/05/16.
//  Copyright © 2016 Wekancode Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface Logger : NSObject

+(id)sharedInstance;

-(void) print:(NSString*)msg class:(NSString*)className type:(LogState)state;

@end

//
//  Helper.m
//  OneLeague
//
//  Created by Wekancode Mohammed on 10/05/16.
//  Copyright © 2016 Wekancode Technologies. All rights reserved.
//

#import "LayoutHelper.h"
#import <CoreData/CoreData.h>

@implementation LayoutHelper

/*!@brief shared instance of the class*/
static LayoutHelper *helper = nil;

#pragma mark Singleton Methods
/*!
 @brief creates the shared instance once
 @return logger
 */
+ (id)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[self alloc] init];
    });
    return helper;
}
#pragma end

/*! @brief shows the alert view
+(void)showAlert:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)buttonTitle {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:buttonTitle
                                              otherButtonTitles:nil];
        
        [alert show];
    });
}
 
*/

/*! @brief To validate email address */

- (BOOL)validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    
    return [emailPred evaluateWithObject:email];
}
-(NSString*)validatePrice:(NSString *)string{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setGeneratesDecimalNumbers:YES];
//    [numberFormatter setMaximumFractionDigits:0];
    [numberFormatter setPositiveFormat:@"¤#,##0.##"];
//    [numberFormatter setformat:@"¤#,##0.00"];
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setCurrencySymbol:@"$"]; // <-- this
    NSString *numberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:[string floatValue]]];
    return numberAsString;
}
/*! @brief method to disable and grey-out any view  */
-(void)disableView:(UIView *)view {
    
    view.userInteractionEnabled = NO;
    view.tintColor = [UIColor lightGrayColor];
    view.alpha = 0.7;
}
/*! @brief method to enable the disabled view  */
-(void)enableView:(UIView *)view {
    
    view.userInteractionEnabled = YES;
    view.tintColor = [UIColor clearColor];
    view.alpha = 1.0;
}
/*! @brief method to add rounded corner border */
-(void)addRoundCornerBorderToView:(UIView *)view {
    view.backgroundColor = [UIColor whiteColor];
    view.layer.masksToBounds = NO;
    view.layer.cornerRadius = 3.0;
//    view.layer.borderColor = [UIColor StallerGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
}

/*! @brief method to add shadow */
-(void)addShadowToView:(UIView *)view {
    view.backgroundColor = [UIColor whiteColor];
    view.layer.masksToBounds = NO;
    view.layer.cornerRadius = 3.0;
    view.layer.shadowOffset = CGSizeMake(1, 2);
    view.layer.shadowOpacity = 0.5;
//    view.layer.shadowRadius = 10.0;

}

/*! @brief method to add shadow */
-(void)addShadowToView:(UIView *)view forCell:(UITableViewCell *)cell{
    cell.contentView.backgroundColor = [UIColor clearColor];
    [self addShadowToView:view];
}

/*! @brief method to add shadow */
-(void)addShadowToCollectionCell:(UIView *)view forCell:(UICollectionViewCell *)cell{
    cell.contentView.backgroundColor = [UIColor clearColor];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.masksToBounds = NO;
    view.layer.cornerRadius = 3.0;
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowOpacity = 0.5;
}

/*! @brief method to calculate days between two dates */
-(int)calculateTotalNumberOfDaysBetween:(NSDate *)date1 and:(NSDate *)date2{
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit
                                                                   fromDate: date1 toDate: date2 options: 0];
    NSInteger days = [components day] + 1;// +1 is to count the start day as well.
    
    NSLog(@"There are %ld days",(long)days);
    
    return (int)days;
}

/*! @brief method to share the imageview circular */
-(void)fixCircularImageView:(UIImageView *)imageView{
    imageView.layer.cornerRadius = imageView.frame.size.height/2;
    imageView.layer.masksToBounds = YES;
    imageView.layer.borderWidth = 0.5;
}

/*! @brief remove data set from Core data
 */
-(void)removeLocalData:(NSString *)entityName{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            NSManagedObjectContext *context = [self managedObjectContext];
            NSFetchRequest *request = [[NSFetchRequest alloc]init];
            [request setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
            [request setIncludesPropertyValues:NO];
            
            NSError *error = nil;
            NSArray *collection = [context executeFetchRequest:request error:&error];
            
            for (NSManagedObject *managedObject in collection) {
                [context deleteObject:managedObject];
            }
            
            NSError *er;
            if(![context save:&er])
                NSLog(@"Error:%@",er.description);
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception:%@",exception.reason);
        }
        
    });
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end

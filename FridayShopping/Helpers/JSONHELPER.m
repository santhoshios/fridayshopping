//
//  JSON.m
//  deliveryBoyTracking
//
//  Created by Media3 on 5/26/16.
//  Copyright © 2016 Media3. All rights reserved.
//

#import "JSONHELPER.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "AFHTTPSessionManager.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <AFNetworking/AFNetworkReachabilityManager.h>

#define USER_TOKEN @"sdjfdsf"
#define BASE_IMAGE_URL @"ddfjdsfk"

@implementation JSONHELPER


+(void)postWithParams:(NSDictionary *)inParams andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure{
    
    @try {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,postApi];
        
        AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        policy.allowInvalidCertificates = YES;
        policy.validatesDomainName = NO;
        sessionManager.securityPolicy = policy;
    
    //    /*These cases Authorization token not needed*/
//        if (![postApi isEqual: SIGNUP] || ![postApi isEqual: VERIFY_OTP] || ![postApi isEqual: RESEND_OTP]|| ![postApi isEqual: LOGIN]||![postApi isEqual: FORGOT_PASS])
        [sessionManager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:USER_TOKEN] forHTTPHeaderField:@"authorization"];
    //
        [sessionManager POST:urlStr parameters:inParams progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [[ParserHelper sharedInstance] checkResponseStatus:responseObject onSuccess:^(NSDictionary *responseDictionary) {
                
                if ([responseObject valueForKey:@"success"]) {
                    completionBlock(responseDictionary);
                }else{
                    failure([responseObject valueForKey:@"errors.msg"]);
                        //    [SVProgressHUD dismiss];
                }
            } onFailure:^(NSString *errorMessage) {
                failure(errorMessage);
                    //  [SVProgressHUD dismiss];
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure (error.localizedDescription);
                //   [SVProgressHUD dismiss];
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"Products Error:%@",exception.reason);
            //  [SVProgressHUD dismiss];
    }
}

+(void)postWithParams:(NSDictionary*)inParams andImageData:(NSData *)imageData andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure{
    
    @try {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,postApi];
        
        AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        policy.allowInvalidCertificates = YES;
        policy.validatesDomainName = NO;
        sessionManager.securityPolicy = policy;
        
        [sessionManager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:USER_TOKEN] forHTTPHeaderField:@"authorization"];
        
        [sessionManager POST:urlStr parameters:inParams constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            [formData appendPartWithFileData:imageData name:@"photo" fileName:@"leagueImage.jpg" mimeType:@"image/jpeg"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [[ParserHelper sharedInstance] checkResponseStatus:responseObject onSuccess:^(NSDictionary *responseDictionary) {
                
                if ([responseObject valueForKey:@"success"]) {
                    completionBlock(responseDictionary);
                }else{
                    failure([responseObject valueForKey:@"errors.msg"]);
                        //   [SVProgressHUD dismiss];
                }
            } onFailure:^(NSString *errorMessage) {
                failure(errorMessage);
                    //  [SVProgressHUD dismiss];
            }];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure (error.localizedDescription);
                //  [SVProgressHUD dismiss];
//            [self setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//                NSLog(@"changed %d", status);
//                //your code here
//            }];
        }];
    
    }
    @catch (NSException *exception) {
        NSLog(@"Products Error:%@",exception.reason);
            // [SVProgressHUD dismiss];
    }
}



+(void)getWithParams:(NSDictionary *)inParams andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure{
    
    @try {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,postApi];
        
        AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        policy.allowInvalidCertificates = YES;
        policy.validatesDomainName = NO;
        sessionManager.securityPolicy = policy;
        sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", nil];
            //  [sessionManager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:USER_TOKEN] forHTTPHeaderField:@"authorization"];

        [sessionManager GET:urlStr parameters:inParams progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            [[ParserHelper sharedInstance] checkResponseStatus:responseObject onSuccess:^(NSDictionary *responseDictionary) {
                
                if ([responseObject valueForKey:@"success"]) {
                    completionBlock(responseDictionary);
                }else{
                    failure([responseObject valueForKey:@"errors.msg"]);
                        //         [SVProgressHUD dismiss];
                }
            } onFailure:^(NSString *errorMessage) {
                failure(errorMessage);
                    //  [SVProgressHUD dismiss];
            }];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure (error.localizedDescription);
                //[SVProgressHUD dismiss];
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"Products Error:%@",exception.reason);
            // [SVProgressHUD dismiss];
    }
    
}
    

+(void)uploadImageWithData:(NSData *)inParams andPostMethod:(NSString *)postApi onCompletion:(JSONResponseBlock)completionBlock onFailure:(APIResponseFailed)failure{
    
    @try {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@%@",BASE_URL,postApi];
        
        AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
        AFSecurityPolicy *policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        policy.allowInvalidCertificates = YES;
        policy.validatesDomainName = NO;
        sessionManager.securityPolicy = policy;

        [sessionManager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:USER_TOKEN] forHTTPHeaderField:@"authorization"];

        [sessionManager POST:urlStr parameters:@{} constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            [formData appendPartWithFileData:inParams name:@"photos" fileName:@"profileimage.jpg" mimeType:@"image/jpeg"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [[ParserHelper sharedInstance] checkResponseStatus:responseObject onSuccess:^(NSDictionary *responseDictionary) {
                
                if ([responseObject valueForKey:@"success"]) {
                    completionBlock(responseDictionary);
                }else{
                    failure([responseObject valueForKey:@"errors.msg"]);
                        //            [SVProgressHUD dismiss];
                }
            } onFailure:^(NSString *errorMessage) {
                failure(errorMessage);
                    //      [SVProgressHUD dismiss];
            }];

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failure (error.localizedDescription);
                //     [SVProgressHUD dismiss];

        }];
        
    }
    @catch (NSException *exception) {
        NSLog(@"Products Error:%@",exception.reason);
            //  [SVProgressHUD dismiss];
    }
}




//+(void)getExecuteWithParams:(NSString *)inParams andPostMethod:(NSString *)inMethod onCompletion:(JSONResponseBlock)completionBlock{
//    
//    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
//    
//    UIDevice *device = [UIDevice currentDevice];
//    NSString  *uuid = [[device identifierForVendor]UUIDString];
//    NSData *postData = [inParams dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    
//    // NSData *postData = [NSKeyedArchiver archivedDataWithRootObject:inParams];
//    
//    
//    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",BASE_URL,inParams]];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:60.0];
//    
//    [request setURL:[NSURL URLWithString:@"http://52.27.200.191/help_rest_api/api.php?"]];
//    
//    
//    [request setHTTPMethod:@"POST"];
//    
//    [request setHTTPBody:postData];
//    
//    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        
//        if (data) {
//            
//            NSError* error;
//            id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
//            if (jsonObject) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    // self.model = jsonObject;
//                    NSLog(@"mmmm: %@", jsonObject);
//                    completionBlock(jsonObject);
//                    
//                });
//            } else {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    //[self handleError:error];
//                    NSLog(@"ERROR: %@", error);
//                    completionBlock(jsonObject);
//                    
//                });
//            }
//        }
//        else {
//            // request failed - error contains info about the failure
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //[self handleError:error]; // execute on main thread!
//                //                completionBlock(jsonObject);
//                // UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Help!" message:@"Please check Network Connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                //[alert show];
//                //                [SVProgressHUD dismiss];
//                NSLog(@"ERROR: %@", error);
//            });
//        }
//    }];
//    [postDataTask resume];
//    
//}
    
+(void)getImageFromApi:(UIImageView *)imgView imageUrl:(NSString*)imgURLString{
        
        UIImage *placeholderImage = [UIImage imageNamed:@"placeholder.png"];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_IMAGE_URL,imgURLString]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        __weak UIImageView *clubImageView = imgView;
        [clubImageView setImageWithURLRequest:request
                             placeholderImage:placeholderImage
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          clubImageView.image = image;
                                      } failure:nil];
        
}

+(void)cancelHttpRequests{
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    [sessionManager invalidateSessionCancelingTasks:YES];
}

-(NSString *)jsonStringFromDictionary:(NSDictionary *)inDict{
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inDict options:0 error:&writeError];
    NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"JSON Input: %@", jsonString);
    return jsonString;
}

+(NSString *)jsonStringFromDictionary:(NSDictionary *)inDict{
    NSError *writeError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inDict options:0 error:&writeError];
    NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSASCIIStringEncoding];
    NSLog(@"JSON Input: %@", [NSString stringWithFormat:@"%@", inDict]);
    return jsonString;
}
    @end

//
//  Helper.h
//  OneLeague
//
//  Created by Wekancode Mohammed on 10/05/16.
//  Copyright © 2016 Wekancode Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"


@interface LayoutHelper : NSObject

+(id)sharedInstance;

//+(void)showAlert:(NSString*)title message:(NSString*)message cancelButtonTitle:(NSString*)buttonTitle ;
- (BOOL)validateEmail:(NSString *)email;
-(void)disableView:(UIView *)view ;
-(void)enableView:(UIView *)view ;
-(void)addRoundCornerBorderToView:(UIView *)view ;
-(void)addShadowToView:(UIView *)view ;
-(void)addShadowToView:(UIView *)view forCell:(UITableViewCell *) cell;
-(void)addShadowToCollectionCell:(UIView *)view forCell:(UICollectionViewCell *)cell;
-(void)fixCircularImageView:(UIImageView *)imageView;
-(void)removeLocalData:(NSString *)entityName;
- (NSManagedObjectContext *)managedObjectContext;
-(int)calculateTotalNumberOfDaysBetween:(NSDate *)date1 and:(NSDate *)date2;
-(NSString*)validatePrice:(NSString *)string;


@end

//
//  ParserHelper.m
//  OneLeague
//
//  Created by Wekancode Mohammed on 10/05/16.
//  Copyright © 2016 Wekancode Technologies. All rights reserved.
//

#import "ParserHelper.h"
#import "Logger.h"

@implementation ParserHelper

/*!@brief shared instance of the class*/
static ParserHelper *helper = nil;

#pragma mark Singleton Methods
/*!
 @brief creates the shared instance once
 @return logger
 */
+ (id)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[self alloc] init];
    });
    return helper;
}
#pragma end

/*!
 @brief get the non null value of from the given 'dictionary' for 'key' else returns enmpty value ""
 @return string value for the key
 */
-(NSString*)getStringFrom:(NSDictionary*)dict forKey:(NSString*)key className:(NSString*)className{
    @try{
        if(dict != nil && ![dict isKindOfClass:[NSNull class]]){
            if([dict objectForKey:key] && ![[dict objectForKey:key] isKindOfClass:[NSNull class]] && [[dict objectForKey:key] isKindOfClass:[NSString class]]){
                return [dict objectForKey:key];
            }else{
                return @"";
                [[Logger sharedInstance] print:@"Key not exists" class:className type:LOGGER_ERROR];
            }
        }else{
            return @"";
            [[Logger sharedInstance] print:@"Key not exists" class:className type:LOGGER_ERROR];
        }
    }@catch(NSException *ex){
        [[Logger sharedInstance] print:ex.reason class:className type:LOGGER_ERROR];
    }
}

/*!
 @brief check whether the response status is success / failure
 */

-(void)checkResponseStatus:(id)responseObject onSuccess:(APIResponseSuccess)success onFailure:(APIResponseFailed)failure {
    
    @try{
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *responseDict = responseObject;
            
            if ([[responseDict objectForKey:@"success"] intValue] == 1) {
                success(responseDict);
            }else if([[responseDict objectForKey:@"success"] intValue] == 2){
                success(responseDict);
            }else if([[responseDict objectForKey:@"success"] intValue] == 3){
                success(responseDict);
            }
            else if ([[responseDict objectForKey:@"success"] intValue] == 0) {
                if([[responseObject valueForKeyPath:@"errors.msg"] isKindOfClass:[NSString class]])
                    failure([responseObject valueForKeyPath:@"errors.msg"]);
                else {
                    NSDictionary *json = [responseObject valueForKeyPath:@"errors"];
                    if ([[json valueForKey:@"msg"] isKindOfClass:[NSArray class]]) {
                        failure([[json valueForKey:@"msg"] firstObject]);
                    } else {
                        failure([json valueForKey:@"msg"]);
                    }
                }
            }
            else {
                if ([responseDict objectForKey:@"msg"]) {
                    failure([responseDict objectForKey:@"msg"]);
                } else {
                    failure(@"Unknown error occurred. Some values seems to be incorrect.");
                }
            }
        } else {
            failure(@"Unnown error occured, please try again");
        }
    }
    @catch(NSException *ex){
        [[Logger sharedInstance] print:ex.reason class:NSStringFromClass([self class]) type:LOGGER_ERROR];
    }
}


@end

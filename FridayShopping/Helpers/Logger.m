//
//  Logger.m
//  OneLeague
//
//  Created by Wekancode Mohammed on 19/05/16.
//  Copyright © 2016 Wekancode Technologies. All rights reserved.
//

#import "Logger.h"

@implementation Logger

/*!@brief shared instance of the class*/
static Logger *logger = nil;

#pragma mark Singleton Methods
/*!
 @brief creates the shared instance once
 @return logger
 */
+ (id)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        logger = [[self alloc] init];
    });
    return logger;
}
#pragma end

#pragma UserDefined
/*!
 @brief prints the message
 @param msg - what to print
 @praram className - class name which print the message
 */
-(void) print:(NSString*)msg class:(NSString*)className type:(LogState)state{
    if (state == LOGGER_ERROR){
        NSLog(@" Class: %@ Error: %@",className,msg);
    }else if(state == LOGGER_DEBUG){
        NSLog(@" Class: %@ Info: %@",className,msg);
    }
}
#pragma end


@end

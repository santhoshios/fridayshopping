//
//  CoreDataStore.m
//  Hawkeye
//
//  Created by WeKanCode on 06/09/16.
//  Copyright © 2016 WeKanCode. All rights reserved.
//

#import "CoreDataStore.h"


@interface CoreDataStore ()


@property (nonatomic,strong,readwrite) NSManagedObjectContext* managedObjectContextForGoldSub;
@property (nonatomic,strong,readwrite) NSManagedObjectContext* mainManagedObjectContext;


@end


@implementation CoreDataStore

static CoreDataStore *_sharedObject;

+ (CoreDataStore*)sharedObject
{
    if( _sharedObject == nil )
        _sharedObject = [[CoreDataStore alloc] init];
    return _sharedObject;
}

- (NSURL *)applicationDocumentsDirectory {
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
}

- (NSManagedObjectModel *)managedObjectModel {
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
   
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"One_League" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
        // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"One_League.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
            // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)mainManagedObjectContext
{
    @synchronized(self) {
        if (_mainManagedObjectContext != nil) {
            return _mainManagedObjectContext;
        }
        
        _mainManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _mainManagedObjectContext.persistentStoreCoordinator = [self persistentStoreCoordinator];
        [_mainManagedObjectContext setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
        return _mainManagedObjectContext;
    }
}


- (NSManagedObjectContext*)newPrivateContext
{
    NSManagedObjectContext* context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        context.persistentStoreCoordinator = self.persistentStoreCoordinator;
        [context setUndoManager:nil];
        [context setMergePolicy:NSMergeByPropertyStoreTrumpMergePolicy];
    return context;
}

-(void) flushDatabase{
    [_mainManagedObjectContext performBlockAndWait:^{
        NSArray *stores = [_persistentStoreCoordinator persistentStores];
        for(NSPersistentStore *store in stores) {
            [_persistentStoreCoordinator removePersistentStore:store error:nil];
            [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
        }
    }];
    [_mainManagedObjectContext reset];
    _persistentStoreCoordinator = nil;
    _mainManagedObjectContext = nil;
    _managedObjectModel = nil;
}

//
//- (void)deleteEntities{
//    
//    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
//    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
//    
//    NSError *deleteError = nil;
//    if ([_persistentStoreCoordinator executeRequest:delete withContext:_mainManagedObjectContext error:&deleteError]) {
//        NSLog(@"delete entity %@",[deleteError localizedDescription]);
//    }
//    request = [[NSFetchRequest alloc] initWithEntityName:@"Event"];
//    delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
//    
//    deleteError = nil;
//    if ([_persistentStoreCoordinator executeRequest:delete withContext:_mainManagedObjectContext error:&deleteError]) {
//        NSLog(@"delete entity %@",[deleteError localizedDescription]);
//    }
//    request = [[NSFetchRequest alloc] initWithEntityName:@"Player"];
//    delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
//    
//    deleteError = nil;
//    if ([_persistentStoreCoordinator executeRequest:delete withContext:_mainManagedObjectContext error:&deleteError]) {
//        NSLog(@"delete entity %@",[deleteError localizedDescription]);
//    }
//    
//    
//}


@end
